import {
    initializeApp
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";

import {
    getAuth,
    createUserWithEmailAndPassword,
    signInWithEmailAndPassword,
    AuthErrorCodes
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

import {
    getDatabase,
    set,
    ref,
    onValue,
    update,
    remove,
    get,
    child
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

const firebaseConfig = {
    apiKey: "AIzaSyCzcCu2fNexrZ_jSXyYIFilZukqCMdmTPk",
    authDomain: "arpfrontpl1.firebaseapp.com",
    databaseURL: "https://arpfrontpl1-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "arpfrontpl1",
    storageBucket: "arpfrontpl1.appspot.com",
    messagingSenderId: "967237604679",
    appId: "1:967237604679:web:d53371509730b9ff166862",
    measurementId: "G-8810P5RVR1"
};

const app = initializeApp(firebaseConfig);

// 1. Inicjalizacja modułu Auth i modułu RealtimeDatabase
const auth = getAuth(app);
const database = getDatabase();

// 2. Definiowanie elementów UI
const emailForm = document.querySelector("#emailForm");
const passwordForm = document.querySelector("#passwordForm");
const nameForm = document.querySelector("#nameForm");
const dateOfBirthForm = document.querySelector("#dateOfBirthForm");
const loginBtn = document.querySelector("#loginBtn");
const signUpBtn = document.querySelector("#signUpBtn");
const userData = document.querySelector("#userData");

// 3. Dodawanie elementów do bazy Realtime
const saveToRealtime = (uid) => {
    const valueEmail = emailForm.value;
    const valueName = nameForm.value;
    const valueDateOfBirth = dateOfBirthForm.value;
    console.log("valueDateOfBirth: " + valueDateOfBirth);

    set(ref(database, "users/" + uid), {
        email: valueEmail,
        name: valueName,
        dateOfBirth: valueDateOfBirth
    });
}

// 4. Dodanie możliwość rejestracji użytkownika
const signUpUser = async() => {
    const valueEmail = emailForm.value;
    const valuePassword = passwordForm.value;

    try {
        const user = await createUserWithEmailAndPassword(auth, valueEmail, valuePassword);
        console.log(user);

        // Dodanie danych do bazy danych (np. Firestore),
        // ale u nas w celach ćwieczniowych wykorzystamy Realtime Database
        const userUid = user.user.uid;
        saveToRealtime(userUid);
    } catch(error) {
        console.warn(error);
    }
};

signUpBtn.addEventListener('click', signUpUser);

const getFromRealtime = (uid) => {
    // Pobieranie jednego konkretnego elementu
    /*const userRef = ref(database, "users/" + uid);

    onValue(userRef, (snapshot) => {
        const userSnapshot = snapshot.val();

        let userInfo = `${userSnapshot.email} ${userSnapshot.name} ${userSnapshot.dateOfBirth}`
        userData.innerHTML += userInfo;
    })*/

    // Pobieranie wszystkich elementów z "folderu"
    const userRef = ref(database, "users");

    onValue(userRef, (snapshot) => {
        const userSnapshot = snapshot.val();
        const userKeys = Object.keys(userSnapshot);
        console.log(userKeys);
    })
}

// 5. Odczytywanie rekordów z Realtime
const loginUser = async() => {
    const valueEmail = emailForm.value;
    const valuePassword = passwordForm.value;

    try {
        // Logowanie
        const user = await signInWithEmailAndPassword(auth, valueEmail, valuePassword);
    
        const userUid = user.user.uid;
        getFromRealtime(userUid);
    } catch(error) {
        // Obsługa nieudanego logowania
        if (error.code === AuthErrorCodes.INVALID_PASSWORD) {
            errorLabel.innerHTML = "Podano nieprawidłowe hasło!";
        } else {
            errorLabel.innerHTML = error.code;
        }
    }
};

loginBtn.addEventListener('click', loginUser);

// 6. Aktualizowanie istniejących wpisów
update(ref(database, "users/EE9spsk6Upg49M7T34mAFuIIGQ73"), {
    name: "zmieniony name",
    dateOfBirth: "01-01-2020"
})

// 7. Usuwanie instniejących wpisów
remove(ref(database, "users/EE9spsk6Upg49M7T34mAFuIIGQ73"));
set(ref(database, "users/06h2oFTi1hOQsWulgdaMnwKZrP12"), null);

// 7.1. Pobranie wszystkich IDków
const usersData = document.querySelector("#usersData");
const allUsersRef = ref(database, "users");
onValue(allUsersRef, (snapshot) => {
    const allUsers = snapshot.val();
    const allUsersUid = Object.keys(allUsers);
    
    allUsersUid.forEach((userUid) => {
        getUserInfo(userUid);
    });
});

// 7.2. Na podstawie pobranych IDków pobrać dane poszczególnych userów
//      i wyświetlić dane w oczekiwanym formacie
const getUserInfo = (uid) => {
    get(child(ref(database), "users/" + uid))
        .then((snapshot) => {
            const userValues = snapshot.val();

            if (userValues.phoneNumber === undefined) {
                userValues.phoneNumber = "BRAK NUMERU TELEFONU";
            }

            const userDescription = `<p>${userValues.name} ma numer telefonu: ${userValues.phoneNumber}</p>`;
            usersData.innerHTML += userDescription;
        })
        .catch((error) => {
            console.warn(error);
        })
}



/*
7. *** Dodaj wyświetlanie listy wszystkich użytkowników w postaci:
userid_1
name: xxx
number: yyy
userid_2
name: xxx
number: yyy

8. *** Dodaj label, który będzie informował, że użytkownik stracił połączenie z internetem.
*/
const connectionStatusRef = ref(database, ".info/connected");
onValue(connectionStatusRef, (snapshot) => {
    console.log("CONNECTION: " + snapshot.val());
});
