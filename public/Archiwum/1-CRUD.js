import {
    initializeApp
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";

import {
    getFirestore,
    doc,
    getDoc,
    getDocs,
    setDoc,
    query,
    collection,
    updateDoc,
    deleteDoc,
    where
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";

const firebaseConfig = {
    apiKey: "AIzaSyCzcCu2fNexrZ_jSXyYIFilZukqCMdmTPk",
    authDomain: "arpfrontpl1.firebaseapp.com",
    projectId: "arpfrontpl1",
    storageBucket: "arpfrontpl1.appspot.com",
    messagingSenderId: "967237604679",
    appId: "1:967237604679:web:d53371509730b9ff166862",
    measurementId: "G-8810P5RVR1"
};

const app = initializeApp(firebaseConfig);

// 1. Podłączenie modułu Firestore
const db = getFirestore(app);

// 2. Zebranie informacji o dokumencie, który chcemy pobrać
const collectionName = "books";
const documentId = "2";


// 3. Pobranie informacji o konkretnej książce
const docRef = doc(db, collectionName, documentId);
const docSnap = await getDoc(docRef);

if (docSnap.exists()) {
    console.log(docSnap.data());
} else {
    console.log('Book does not exist!');
}

/*
// 4. Dodawanie dokumentu do bazy danych
const bookRef = doc(db, collectionName, "2");
const book = {
    title: "Pan Tadeusz",
    price: 15,
    isAvailable: false,
    genres: ["poem"]
};

setDoc(bookRef, book);

// 5. Pobieranie informacji o wszystkich książkach,
//    których status isAvailable jest równy True
const q = query(collection(db, collectionName), where("isAvailable", "==", true));
// Pobranie wszystkich elementów z kolekcji
const querySnap = await getDocs(q);

// Przejście przez wszystkie pobrane wartości
querySnap.forEach((docx) => {
    // Wyświetlanie ID dokumentu
    console.log(docx.id);
    // Wyświetlanie danych dokumentu
    console.log(docx.data());
});

// 6. Aktualizowanie istniejących już dokumentów
const bookForUpdateRef = doc(db, collectionName, "2");
const bookForUpdate = {
    isAvailable: false
};

await updateDoc(bookForUpdateRef, bookForUpdate);
*/

// 7. Usuwanie dokumentów
// await deleteDoc(doc(db, collectionName, "2"));

// 8*. Stworzenie UI do wyświetlania pobranych dokumentów z bazy danych
// 8.1. Pobranie wszystkich książek
// 8.2. Zebrać książki w postaci listy
// 8.3. Umieścić listę w HTMLu

// 9**. Dodanie możliwości usuwanie rekordu z poziomu UI

const booksListDiv = document.querySelector("#booksList");

const deleteBook = async(bookId) => {
    const id = bookId.target.dataset.id;
    deleteDoc(doc(db, collectionName, id));

    booksListDiv.innerHTML = "";
    initBooksList();
};

const initBooksList = async() => {
    const queryAllBooks = query(collection(db, collectionName));
    const allBooks = await getDocs(queryAllBooks);

    let booksList = "<ul>";
    allBooks.forEach((book) => {
        const bookTitle = book.data().title;
        const bookId = book.id;

        booksList += `<li> ${bookTitle}`;
        booksList += `<button type="button" data-id="${bookId}" class='btnDel'>Delete</button>`
        booksList += `</li>`;
    });
    booksList += `</ul>`;

    booksListDiv.innerHTML = booksList;

    const deleteBtns = document.querySelectorAll(".btnDel");
    deleteBtns.forEach((singleDeleteBtn) => {
        singleDeleteBtn.addEventListener("click", deleteBook);
    })
};

initBooksList();

