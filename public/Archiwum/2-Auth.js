import {
    initializeApp
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";

import {
    getAuth,
    signInWithEmailAndPassword,
    AuthErrorCodes,
    createUserWithEmailAndPassword,
    signOut,
    onAuthStateChanged,
    GoogleAuthProvider,
    signInWithPopup 
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

import {
    getFirestore,
    doc,
    getDoc,
    getDocs,
    setDoc,
    query,
    collection,
    updateDoc,
    deleteDoc,
    where
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";

const firebaseConfig = {
    apiKey: "AIzaSyCzcCu2fNexrZ_jSXyYIFilZukqCMdmTPk",
    authDomain: "arpfrontpl1.firebaseapp.com",
    projectId: "arpfrontpl1",
    storageBucket: "arpfrontpl1.appspot.com",
    messagingSenderId: "967237604679",
    appId: "1:967237604679:web:d53371509730b9ff166862",
    measurementId: "G-8810P5RVR1"
};

const app = initializeApp(firebaseConfig);

// 1. Inicjalizacja modułu Auth
const auth = getAuth(app);
const googleAuth = new GoogleAuthProvider();

// 1.1. Inicjalizacja modułu bazy danych
const db = getFirestore(app);
const collectionName = "books";

// 2. Definiowanie elementów UI
const emailForm = document.querySelector("#emailForm");
const passwordForm = document.querySelector("#passwordForm");
const loginBtn = document.querySelector("#loginBtn");
const signUpBtn = document.querySelector("#signUpBtn");
const errorLabel = document.querySelector("#errorLabel");
const authUser = document.querySelector("#authUser");
const logOutBtn = document.querySelector("#logOutBtn");
const loginWithGoogleBtn = document.querySelector("#loginWithGoogleBtn");
const booksListDiv = document.querySelector("#booksListDiv");

// 3. Dodanie logowania
const loginUser = async() => {
    const valueEmail = emailForm.value;
    const valuePassword = passwordForm.value;

    try {
        // Logowanie
        const user = await signInWithEmailAndPassword(auth, valueEmail, valuePassword);
        console.log(user);
    } catch(error) {
        // Obsługa nieudanego logowania
        if (error.code === AuthErrorCodes.INVALID_PASSWORD) {
            errorLabel.innerHTML = "Podano nieprawidłowe hasło!";
        } else {
            errorLabel.innerHTML = error.code;
        }
    }
};

loginBtn.addEventListener('click', loginUser);

// 4. Dodanie możliwość rejestracji użytkownika
const signUpUser = async() => {
    const valueEmail = emailForm.value;
    const valuePassword = passwordForm.value;

    try {
        const user = await createUserWithEmailAndPassword(auth, valueEmail, valuePassword);
        console.log(user);

        // Dodanie danych do bazy danych (np. Firestore)
    } catch(error) {
        console.warn(error);
    }
};

signUpBtn.addEventListener('click', signUpUser);

// 5. Dodanie możliwości wylogowywania użytkownika
const logoutUser = async() => {
    await signOut(auth);
    console.log("User logged out");
};

logOutBtn.addEventListener('click', logoutUser);

// 6. Dodanie funkcji nasłuchującej na zmianę statusu sesji użytkownika
const authUserObserver = async() => {
    onAuthStateChanged(auth, user => {
        if (user) {
            // Tu rozpoznaliśmy, że ktoś jest uwierzytelniony
            loginForm.style.display = 'none';
            authUser.style.display = 'block';
            initBooksList();
        } else {
            loginForm.style.display = 'block';
            authUser.style.display = 'none';
            booksListDiv.innerHTML = '';
        }
    });
};
authUserObserver();

// 7. Dodanie możliwości uwierzytelnienia się poprzez konto Google
loginWithGoogleBtn.onclick = async() => {
    signInWithPopup(auth, googleAuth)
        .then((result) => {
            // This gives you a Google Access Token. You can use it to access the Google API.
            const credential = GoogleAuthProvider.credentialFromResult(result);
            const token = credential.accessToken;
            // The signed-in user info.
            const user = result.user;
            console.log(user);
            // ...
        }).catch((error) => {
            // Handle Errors here.
            const errorCode = error.code;
            const errorMessage = error.message;
            // The email of the user's account used.
            const email = error.customData.email;
            // The AuthCredential type that was used.
            const credential = GoogleAuthProvider.credentialFromError(error);
            // ...
            console.warn(errorMessage);
        });
};

const deleteBook = async(bookId) => {
    const id = bookId.target.dataset.id;
    deleteDoc(doc(db, collectionName, id));

    booksListDiv.innerHTML = "";
    initBooksList();
};

const updateBook = async(bookId) => {
    const id = bookId.target.dataset.id;
    const bookForUpdateRef = doc(db, collectionName, id);

    const bookPriceInput = document.querySelector(`#input${id}`)
    const bookForUpdate = {
        price: bookPriceInput.value
    };

    await updateDoc(bookForUpdateRef, bookForUpdate);
}

const initBooksList = async() => {
    const queryAllBooks = query(collection(db, collectionName));
    const allBooks = await getDocs(queryAllBooks);

    let booksList = "<ul>";
    allBooks.forEach((book) => {
        const bookData = book.data();
        const bookTitle = bookData.title;
        const bookPrice = bookData.price;
        const bookId = book.id;

        booksList += `<li> ${bookTitle}`;
        booksList += `<button type="button" data-id="${bookId}" class='btnDel'>Delete</button>`
        booksList += `<input id="input${bookId}" type="number" value="${bookPrice}">`
        booksList += `<button type="button" data-id="${bookId}" class="btnUpdate">Update</button>`
        booksList += `</li>`;
    });
    booksList += `</ul>`;

    booksListDiv.innerHTML = booksList;

    const deleteBtns = document.querySelectorAll(".btnDel");
    deleteBtns.forEach((singleDeleteBtn) => {
        singleDeleteBtn.addEventListener("click", deleteBook);
    })

    const updateBtns = document.querySelectorAll(".btnUpdate");
    updateBtns.forEach((singleUpdateBtn) => {
        singleUpdateBtn.addEventListener("click", updateBook);
    })
};

/*
11*. Pokaż zalogowanym użytkownikom zawartość kolekcji stworzonej w bazie danych.
-> Rozpoznać, czy ktoś jest zalogowany
-> Jeżeli jest to wywołać funkcję, która uzupełni HTML o zawartość kolekcji

12*. Dodanie możliwości usuwanie dokumentu z poziomu UI zalogowanym użytkownikom
-> Rozpoznać, czy ktoś jest zalogowany
-> Napisać funkcję, która przyjmie documentId, a poźniej na podstawie tego IDka usunie dokument z bazy
-> Przy pobieraniu listy elementów dodać też przycisk, który pozwoli na usunięcie danego dokumentu
(tzn. do przycisku podepniemy funkcję, która przyjmie jako parametr ID dokumentu)

13*. Dodanie możliwości edycji dokumentu z poziomu UI zalogowanym użytkownikom
-> Rozpoznać, czy ktoś jest zalogowany
-> Napisać funkcję, która przyjmie documentID oraz wartość parametru/parametrów, który chcemy modyfikować
-> Przy pobieraniu listy elementów dodać też przycisk, który pozwoli na edycję danego dokumentu
(tzn. do przycisku podepniemy funkcję, która przyjmie jako parametr ID dokumentu oraz wartości z inputów)
*/

